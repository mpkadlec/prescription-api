require 'unit/test_helper'
require 'ostruct'
require 'medications/generics_finder.rb'
require 'json/ext'

describe Medications::GenericsFinder do

  it 'should get the correct response' do
    mock_all_medications = [{id: 11, generic: false, active: true, rxcui: '12345'}, {id: 12, 'generic': true, 'active': true, 'rxcui': '12345'}]
    mock_all_prescriptions = [{'id': 1, 'medication_id': 11}]
    expected_response = {prescription_updates: [prescription_id: 1, medication_id: 12]}

    find_generics = Medications::GenericsFinder.new
    find_generics.stub(:all_medications, mock_all_medications) do
      find_generics.stub(:all_prescriptions, mock_all_prescriptions) do
        response = find_generics.find
        assert_equal response, expected_response
      end
    end
  end

  it 'should get no response' do
    mock_all_medications = [{id: 11, generic: false, active: true, rxcui: '12345'}, {id: 12, 'generic': true, 'active': false, 'rxcui': '12345'}]
    mock_all_prescriptions = [{'id': 1, 'medication_id': 11}]
    expected_response = {prescription_updates: []}

    find_generics = Medications::GenericsFinder.new
    find_generics.stub(:all_medications, mock_all_medications) do
      find_generics.stub(:all_prescriptions, mock_all_prescriptions) do
        response = find_generics.find
        assert_equal response, expected_response
      end
    end
  end
end



