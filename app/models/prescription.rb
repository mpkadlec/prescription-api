require 'json'
require 'api_request'

class Prescription

  PRESCRIPTIONS_URL = 'http://api-sandbox.pillpack.com/prescriptions'

  def self.all
    ApiRequest.get_json(PRESCRIPTIONS_URL)
  end

  def self.get(prescription_id)
    ApiRequest.get_json("#{PRESCRIPTIONS_URL}/#{prescription_id}")
  end

end

