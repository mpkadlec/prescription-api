require 'api_request'
require 'json'

class Medication

  MEDICATIONS_URL = 'http://api-sandbox.pillpack.com/medications'

  def self.all
    ApiRequest.get_json(MEDICATIONS_URL)
  end

  def self.get(medication_id)
    ApiRequest.get_json("#{MEDICATIONS_URL}/#{medication_id}")
  end

end
