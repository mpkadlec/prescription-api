require 'faraday'
require 'json'

class ApiRequest

  def self.get_json(url)
    response = Faraday.new(url: url).get
    return nil unless response&.body && response.env.status == 200

    response_json = JSON.parse(response.body, {:symbolize_names => true})
    response_json
  end

end
