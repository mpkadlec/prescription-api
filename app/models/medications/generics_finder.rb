require 'prescription.rb'
require 'medication.rb'
require 'medications/finder.rb'

module Medications
  class GenericsFinder < Medications::Finder

    attr_reader :medications

    def find
      @medications = all_medications
      return 'There was an error retrieving the medications.' if self.medications.nil?

      {prescription_updates: prescription_updates}
    end

    private

    def prescription_updates
      updates = []

      all_prescriptions.each do |prescription|
        brand_medication = find_brand_medication_by_id(self.medications, prescription[:medication_id])
        add_generic_equivalents(brand_medication, prescription, updates) if brand_medication
      end

      updates
    end

    def add_generic_equivalents(brand_medication, prescription, updates)
      generic_equivalents = generic_medications(self.medications, brand_medication)
      generic_equivalents.each do |generic|
        updates << {prescription_id: prescription[:id], medication_id: generic[:id]}
      end
    end

    def all_medications
      Medication.all
    end

    def all_prescriptions
      Prescription.all
    end

  end
end
