module Medications
  class Finder

    def find_brand_medication_by_id(medications, medication_id)
      medications = brand_name_medications(medications, medication_id)
      medications.count > 0 ? medications.first : nil
    end

    def brand_name_medications(medications, medication_id)
      medications.select {|medication| medication[:id] == medication_id &&
          medication[:generic] == false && medication[:active] == true}
    end

    def generic_medications(medications, brand_name_medication)
      medications.select {|medication| medication[:rxcui] == brand_name_medication[:rxcui] &&
          medication[:generic] == true && medication[:active] == true}
    end

  end
end
