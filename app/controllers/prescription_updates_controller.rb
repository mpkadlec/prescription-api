require 'json'
require 'medications/generics_finder.rb'

class PrescriptionUpdatesController < ApplicationController

  def index
    generics = Medications::GenericsFinder.new.find
    render :json => generics
  end

end
